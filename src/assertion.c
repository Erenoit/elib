#include "assertion.h"

#include <b_stacktrace.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>

void elib_assert_failed(const char *condition, const char *file, int line, const char *message,
                        ...) {
    fprintf(stderr, "Assertion failed: %s, file %s, line %d\n\n", condition, file, line);

    va_list args;
    va_start(args, message);
    vfprintf(stderr, message, args);
    va_end(args);

    fprintf(stderr, "\n\n");
    char *stacktrace = b_stacktrace_get_string();
    fprintf(stderr, "stacktrace:\n%s\n\n", stacktrace);
    abort();
}

void elib_abort(const char *type, const char *file, int line, const char *message, ...) {
    fprintf(stderr, "%s: file %s, line %d\n\n", type, file, line);

    va_list args;
    va_start(args, message);
    vfprintf(stderr, message, args);
    va_end(args);

    fprintf(stderr, "\n\n");
    char *stacktrace = b_stacktrace_get_string();
    fprintf(stderr, "stacktrace:\n%s\n\n", stacktrace);
    abort();
}
