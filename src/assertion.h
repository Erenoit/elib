#ifndef __ELIB_ASSERTION_H__
#define __ELIB_ASSERTION_H__

#define B_STACKTRACE_IMPL

#define ELIB_ASSERT(condition, ...)                                      \
    if(ELIB_UNLIKELY(!(condition))) {                                    \
        elib_assert_failed(#condition, __FILE__, __LINE__, __VA_ARGS__); \
    }

#define ELIB_ABORT(...)         elib_abort("Abort", __FILE__, __LINE__, __VA_ARGS__)
#define ELIB_UNREACHABLE(...)   elib_abort("Unreachable", __FILE__, __LINE__, __VA_ARGS__)
#define ELIB_UNIMPLEMENTED(...) elib_abort("Unimplemented", __FILE__, __LINE__, __VA_ARGS__)

#if DEBUG
#define ELIB_DEBUG_ASSERT(condition, ...) ELIB_ASSERT(condition, __VA_ARGS__)
#else
#define ELIB_DEBUG_ASSERT(condition, ...) ((void) 0)
#endif // ELIB_DEBUG_ASSERTIONS

void elib_assert_failed(
    const char *condition, const char *file, int line, const char *message, ...
);
void elib_abort(const char *type, const char *file, int line, const char *message, ...);

#if __GNUC__
#define ELIB_LIKELY(x)   __builtin_expect((x), 1)
#define ELIB_UNLIKELY(x) __builtin_expect((x), 0)
#else
#define ELIB_LIKELY(x)   (x)
#define ELIB_UNLIKELY(x) (x)
#endif // __GNUC__

#if DEBUG
#if _WIN32
#define ELIB_BREAKPOINT() __debugbreak();
#else
#define ELIB_BREAKPOINT() __builtin_trap();
#endif
#else
#define ELIB_BREAKPOINT()
#endif

#endif // __ELIB_ASSERTION_H__
