#include "memory.h"

void elib_memory_memswap(void *first, void *second, size_t size) {
    size_t *first_  = first;
    size_t *second_ = second;
    size_t  tmp;

    while(size--) {
        tmp        = *first_;
        *first_++  = *second_;
        *second_++ = tmp;
    }
}
