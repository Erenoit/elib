#ifndef __ELIB_COLLECTIONS_DYNAMICARRAY_H__
#define __ELIB_COLLECTIONS_DYNAMICARRAY_H__

#include <stdbool.h>
#include <stdlib.h>

/**
 * @brief A dynamic array.
 */
typedef void ElibDynamicArray;

/**
 * @brief Creates a new dynamic array.
 * @param element_size The size of each element in the array.
 * @return A new dynamic array, or NULL if the array could not be created.
 */
ElibDynamicArray *elib_collections_dynamicarray_create(size_t element_size);

/**
 * @brief Destroys a dynamic array.
 * @param array The dynamic array to destroy.
 */
void elib_collections_dynamicarray_destroy(ElibDynamicArray *array);

/**
 * @brief Pushes an element onto the end of the dynamic array.
 * @param array The dynamic array to push onto.
 * @param element The element to push.
 * @return True if the element was pushed, false otherwise.
 */
bool elib_collections_dynamicarray_push(ElibDynamicArray *array, void *element);

/**
 * @brief Pops an element off the end of the dynamic array.
 * @param array The dynamic array to pop from.
 * @return The element that was popped, or NULL if the array was empty.
 */
void *elib_collections_dynamicarray_pop(ElibDynamicArray *array);

/**
 * @brief Appends an array of elements onto the end of the dynamic array.
 * @param array The dynamic array to append onto.
 * @param elements The array of elements to append.
 * @param nitems The number of elements to append.
 * @return True if the elements were appended, false otherwise.
 */
bool elib_collections_dynamicarray_append(ElibDynamicArray *array, void **elements, size_t nitems);

/**
 * @brief Sets an element at a given index in the dynamic array.
 * @param array The dynamic array to set in.
 * @param index The index to set at.
 * @param element The element to set.
 * @return The element that was set, or NULL if the index was out of bounds.
 */
void *elib_collections_dynamicarray_set(ElibDynamicArray *array, size_t index, void *element);

/**
 * @brief Gets an element at a given index in the dynamic array.
 * @param array The dynamic array to get from.
 * @param index The index to get at.
 * @return The element that was gotten, or NULL if the index was out of bounds.
 */
void *elib_collections_dynamicarray_get(ElibDynamicArray *array, size_t index);

/**
 * @brief Inserts an element at a given index in the dynamic array.
 * @param array The dynamic array to insert into.
 * @param index The index to insert at.
 * @param element The element to insert.
 * @return The element that was inserted, or NULL if the index was out of bounds.
 */
void *elib_collections_dynamicarray_insert(ElibDynamicArray *array, size_t index, void *element);

/**
 * @brief Removes an element at a given index in the dynamic array.
 * @param array The dynamic array to remove from.
 * @param index The index to remove at.
 * @return The element that was removed, or NULL if the index was out of bounds.
 */
void *elib_collections_dynamicarray_remove(ElibDynamicArray *array, size_t index);

/**
 * @brief Gets the first element in the dynamic array.
 * @param array The dynamic array to get from.
 * @return The first element in the dynamic array, or NULL if the array is empty.
 */
void *elib_collections_dynamicarray_first(ElibDynamicArray *array);

/**
 * @brief Gets the last element in the dynamic array.
 * @param array The dynamic array to get from.
 * @return The last element in the dynamic array, or NULL if the array is empty.
 */
void *elib_collections_dynamicarray_last(ElibDynamicArray *array);

/**
 * @brief Gets the dynamic array as a regular array.
 * @param array The dynamic array to get from.
 * @return The dynamic array as a regular array.
 */
void **elib_collections_dynamicarray_as_array(ElibDynamicArray *array);

/**
 * @brief Gets the size of the dynamic array.
 * @param array The dynamic array to get from.
 * @return The size of the dynamic array.
 */
size_t elib_collections_dynamicarray_size(ElibDynamicArray *array);

/**
 * @brief Gets the capacity of the dynamic array.
 * @param array The dynamic array to get from.
 * @return The capacity of the dynamic array.
 */
size_t elib_collections_dynamicarray_capacity(ElibDynamicArray *array);

/**
 * @brief Trims the capacity of the dynamic array to its size.
 * @param array The dynamic array to trim.
 */
void elib_collections_dynamicarray_trim_capacity(ElibDynamicArray *array);

#endif // __ELIB_COLLECTIONS_DYNAMICARRAY_H__
