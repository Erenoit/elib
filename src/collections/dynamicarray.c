#include "dynamicarray.h"

#include <assert.h>

#define DEFAULT_CAPACITY 32

typedef struct DynamicArray {
    size_t size;
    size_t nitems;
    size_t capacity;
    void **items;
} DynamicArray;

ElibDynamicArray *elib_collections_dynamicarray_create(size_t element_size) {
    DynamicArray *array = malloc(sizeof(*array));

    if(array == NULL) return NULL;

    array->items    = calloc(DEFAULT_CAPACITY, sizeof(array->items));
    array->size     = element_size;
    array->nitems   = 0;
    array->capacity = DEFAULT_CAPACITY;

    return (ElibDynamicArray *) array;
}

void elib_collections_dynamicarray_destroy(ElibDynamicArray *array) {
    assert(array != NULL);

    DynamicArray *array_ = (DynamicArray *) array;

    for(size_t i = 0; i < array_->nitems; i++) { free(array_->items[i]); }

    free(array_->items);
    free(array_);
}

bool elib_collections_dynamicarray_push(ElibDynamicArray *array, void *element) {
    assert(array != NULL);

    DynamicArray *array_ = (DynamicArray *) array;

    // TODO: is there better constant than 2?
    if(array_->nitems == array_->capacity) {
        array_->capacity *= 2;
        array_->items     = realloc(array_->items, array_->capacity * sizeof(array_->items));
    }

    array_->items[array_->nitems++] = element;

    return true;
}

void *elib_collections_dynamicarray_pop(ElibDynamicArray *array) {
    assert(array != NULL);

    DynamicArray *array_ = (DynamicArray *) array;

    if(array_->nitems == 0) return NULL;

    void *element = array_->items[--array_->nitems];

    array_->items[array_->nitems] = NULL;

    return element;
}

bool elib_collections_dynamicarray_append(ElibDynamicArray *array, void **elements, size_t nitems) {
    assert(array != NULL);

    DynamicArray *array_ = (DynamicArray *) array;

    if(array_->nitems + nitems > array_->capacity) {
        array_->capacity = array_->nitems + nitems;
        array_->items    = realloc(array_->items, array_->capacity * sizeof(array_->items));
    }

    for(size_t i = 0; i < nitems; i++) {
        if(elements[i] == NULL) {
            for(size_t j = 0; j < i; j++) {
                free(array_->items[j]);
                array_->items[j] = NULL;
            }

            return false;
        }

        array_->items[array_->nitems++] = elements[i];
    }

    return true;
}

void *elib_collections_dynamicarray_set(ElibDynamicArray *array, size_t index, void *element) {
    assert(array != NULL);

    DynamicArray *array_ = (DynamicArray *) array;

    if(index >= array_->nitems) return NULL;

    void *old_element = array_->items[index];

    array_->items[index] = element;

    return old_element;
}

void *elib_collections_dynamicarray_get(ElibDynamicArray *array, size_t index) {
    assert(array != NULL);

    DynamicArray *array_ = (DynamicArray *) array;

    if(index >= array_->nitems) return NULL;

    return array_->items[index];
}

void *elib_collections_dynamicarray_insert(ElibDynamicArray *array, size_t index, void *element) {
    assert(array != NULL);

    DynamicArray *array_ = (DynamicArray *) array;

    if(index > array_->nitems) return NULL;

    if(array_->nitems == array_->capacity) {
        array_->capacity *= 2;
        array_->items     = realloc(array_->items, array_->capacity * sizeof(array_->items));
    }

    for(size_t i = array_->nitems; i > index; i--) { array_->items[i] = array_->items[i - 1]; }

    array_->items[index] = element;
    array_->nitems++;

    return element;
}

void *elib_collections_dynamicarray_remove(ElibDynamicArray *array, size_t index) {
    assert(array != NULL);

    DynamicArray *array_ = (DynamicArray *) array;

    if(index >= array_->nitems) return NULL;

    void *element = array_->items[index];

    for(size_t i = index; i < array_->nitems - 1; i++) { array_->items[i] = array_->items[i + 1]; }

    array_->items[--array_->nitems] = NULL;

    return element;
}

void *elib_collections_dynamicarray_first(ElibDynamicArray *array) {
    assert(array != NULL);

    DynamicArray *array_ = (DynamicArray *) array;

    if(array_->nitems == 0) return NULL;

    return array_->items[0];
}

void *elib_collections_dynamicarray_last(ElibDynamicArray *array) {
    assert(array != NULL);

    DynamicArray *array_ = (DynamicArray *) array;

    if(array_->nitems == 0) return NULL;

    return array_->items[array_->nitems - 1];
}

void **elib_collections_dynamicarray_as_array(ElibDynamicArray *array) {
    assert(array != NULL);

    DynamicArray *array_ = (DynamicArray *) array;

    return array_->items;
}

size_t elib_collections_dynamicarray_size(ElibDynamicArray *array) {
    assert(array != NULL);

    DynamicArray *array_ = (DynamicArray *) array;

    return array_->nitems;
}

size_t elib_collections_dynamicarray_capacity(ElibDynamicArray *array) {
    assert(array != NULL);

    DynamicArray *array_ = (DynamicArray *) array;

    return array_->capacity;
}

void elib_collections_dynamicarray_trim_capacity(ElibDynamicArray *array) {
    assert(array != NULL);

    DynamicArray *array_ = (DynamicArray *) array;

    if(array_->nitems == array_->capacity) return;

    array_->capacity = array_->nitems;
    array_->items    = realloc(array_->items, array_->capacity * sizeof(array_->items));
}
