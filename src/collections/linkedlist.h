#ifndef __ELIB_COLLECTIONS_LINKEDLIST_H__
#define __ELIB_COLLECTIONS_LINKEDLIST_H__

#include <stdbool.h>
#include <stdlib.h>

/**
 * @brief A doubly linked list.
 */
typedef void ElibLinkedList;

/**
 * @brief Creates a new linked list.
 * @return A new linked list.
 */
ElibLinkedList *elib_collections_linkedlist_create(void);

/**
 * @brief Destroys a linked list.
 * @param linkedlist The linked list to destroy.
 */
void elib_collections_linkedlist_destroy(ElibLinkedList *linkedlist);

/**
 * @brief Pushes a value onto the start of a linked list.
 * @param linkedlist The linked list to push onto.
 * @param value The value to push.
 * @return True if the value was pushed, false otherwise.
 */
bool elib_collections_linkedlist_push(ElibLinkedList *linkedlist, void *value);

/**
 * @brief Pops a value off the start of a linked list.
 * @param linkedlist The linked list to pop from.
 * @return The value that was popped, or NULL if the linked list is empty.
 */
void *elib_collections_linkedlist_pop(ElibLinkedList *linkedlist);

/**
 * @brief inserts a value into given index of a linked list.
 * @param linkedlist The linked list to insert into.
 * @param index The index to insert into.
 * @param value The value to insert.
 * @return True if the value was inserted, false otherwise.
 */
bool elib_collections_linkedlist_insert(ElibLinkedList *linkedlist, size_t index, void *value);

/**
 * @brief Removes a value from a given index of a linked list.
 * @param linkedlist The linked list to remove from.
 * @param index The index to remove from.
 * @return The value that was removed, or NULL if the linked list is empty.
 */
void *elib_collections_linkedlist_remove(ElibLinkedList *linkedlist, size_t index);

/**
 * @brief Gets a value from a given index of a linked list.
 * @param linkedlist The linked list to get from.
 * @param index The index to get from.
 * @return The value at the given index, or NULL if the linked list is empty.
 */
void *elib_collections_linkedlist_get(ElibLinkedList *linkedlist, size_t index);

/**
 * @brief Sets a value at a given index of a linked list.
 * @param linkedlist The linked list to set in.
 * @param index The index to set at.
 * @param value The value to set.
 * @return The value that was replaced, or NULL if the linked list is empty.
 */
void *elib_collections_linkedlist_set(ElibLinkedList *linkedlist, size_t index, void *value);

/**
 * @brief Gets the size of a linked list.
 * @param linkedlist The linked list to get the size of.
 * @return The size of the linked list.
 */
size_t elib_collections_linkedlist_size(ElibLinkedList *linkedlist);

#endif // __ELIB_COLLECTIONS_LINKEDLIST_H__
