#ifndef __ELIB_COLLECTIONS_HASHTABLE_H__
#define __ELIB_COLLECTIONS_HASHTABLE_H__

#include <stdbool.h>
#include <stdlib.h>

/**
 * @brief A hashtable.
 */
typedef void ElibHashTable;

/**
 * @brief A hash function.
 * @param key The key to hash.
 * @param size The size of the key.
 * @return The hash of the key.
 */
typedef size_t (*ElibHashFunc)(void *key, size_t size);

/**
 * @brief Creates a new hashtable.
 * @param hash_func The hash function to use. If NULL, the default hash function will be used.
 * @return A new hashtable.
 */
ElibHashTable *elib_collections_hashtable_create(ElibHashFunc hash_func);

/**
 * @brief Destroys a hashtable.
 * @param hashtable The hashtable to destroy.
 */
void elib_collections_hashtable_destroy(ElibHashTable *hashtable);

/**
 * @brief Inserts a key-value pair into the hashtable.
 * @param hashtable The hashtable to insert into.
 * @param key The key to insert.
 * @param key_size The size of the key.
 * @param value The value to insert.
 * @return True if the key-value pair was inserted, false otherwise.
 */
bool elib_collections_hashtable_insert(ElibHashTable *hashtable, void *key, size_t key_size,
                                       void *value);

/**
 * @brief Looks up a key in the hashtable.
 * @param hashtable The hashtable to look up in.
 * @param key The key to look up.
 * @param key_size The size of the key.
 * @return The value associated with the key, or NULL if the key was not found.
 */
void *elib_collections_hashtable_lookup(ElibHashTable *hashtable, void *key, size_t key_size);

/**
 * @brief Removes a key-value pair from the hashtable.
 * @param hashtable The hashtable to remove from.
 * @param key The key to remove.
 * @param key_size The size of the key.
 * @return The value associated with the key, or NULL if the key was not found.
 */
void *elib_collections_hashtable_remove(ElibHashTable *hashtable, void *key, size_t key_size);

/**
 * @brief gets array of keys in hashtable
 * @param hashtable The hashtable to check.
 * @return array of keys in hashtable
 */
void *elib_collections_hashtable_keys(ElibHashTable *hashtable);

/**
 * @brief gets array of values in hashtable
 * @param hashtable The hashtable to check.
 * @return array of values in hashtable
 */
void *elib_collections_hashtable_values(ElibHashTable *hashtable);

/**
 * @brief gets size of hashtable
 * @param hashtable The hashtable to check.
 * @return size of hashtable
 */
size_t elib_collections_hashtable_size(ElibHashTable *hashtable);

/**
 * @brief gets hash function of hashtable
 * @param hashtable The hashtable to check.
 * @return hash function of hashtable
 */
ElibHashFunc elib_collections_hashtable_hash_func(ElibHashTable *hashtable);

#endif // __ELIB_COLLECTIONS_HASHTABLE_H__
