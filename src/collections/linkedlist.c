#include "linkedlist.h"

#include <stdlib.h>

typedef struct LinkedList {
    void              *value;
    struct LinkedList *next;
    struct LinkedList *prev;
} LinkedList;

ElibLinkedList *elib_collections_linkedlist_create(void) {
    LinkedList *list = malloc(sizeof(*list));

    list->value = NULL;
    list->next  = NULL;
    list->prev  = NULL;

    return (ElibLinkedList *) list;
}

void elib_collections_linkedlist_destroy(ElibLinkedList *list) {
    LinkedList *l = (LinkedList *) list;

    while(l->next != NULL) {
        LinkedList *next = l->next;
        free(l);
        l = next;
    }

    free(l);
}

bool elib_collections_linkedlist_push(ElibLinkedList *linkedlist, void *value) {
    LinkedList *list = (LinkedList *) linkedlist;

    LinkedList *new = malloc(sizeof(*new));
    new->value      = value;
    new->next       = list->next;
    new->prev       = list;

    return true;
}

void *elib_collections_linkedlist_pop(ElibLinkedList *linkedlist) {
    LinkedList *list = (LinkedList *) linkedlist;

    LinkedList *element = list->next;
    void       *value   = element->value;

    list->next          = element->next;
    element->next->prev = list;

    free(element);

    return value;
}

bool elib_collections_linkedlist_insert(ElibLinkedList *linkedlist, size_t index, void *value) {
    LinkedList *list = (LinkedList *) linkedlist;

    LinkedList *element = list->next;

    while(index--) {
        if(element == NULL) { return false; }

        element = element->next;
    }

    LinkedList *new = malloc(sizeof(*new));
    new->value      = value;
    new->next       = element;
    new->prev       = element->prev;

    element->prev->next = new;
    element->prev       = new;

    return true;
}

void *elib_collections_linkedlist_remove(ElibLinkedList *linkedlist, size_t index) {
    LinkedList *list = (LinkedList *) linkedlist;

    LinkedList *element = list->next;

    while(index--) {
        if(element == NULL) { return NULL; }

        element = element->next;
    }

    void *value = element->value;

    element->prev->next = element->next;
    element->next->prev = element->prev;

    free(element);

    return value;
}

void *elib_collections_linkedlist_get(ElibLinkedList *linkedlist, size_t index) {
    LinkedList *list = (LinkedList *) linkedlist;

    LinkedList *element = list->next;

    while(index--) {
        if(element == NULL) { return NULL; }

        element = element->next;
    }

    return element->value;
}

void *elib_collections_linkedlist_set(ElibLinkedList *linkedlist, size_t index, void *value) {
    LinkedList *list = (LinkedList *) linkedlist;

    LinkedList *element = list->next;

    while(index--) {
        if(element == NULL) { return NULL; }

        element = element->next;
    }

    void *old      = element->value;
    element->value = value;

    return old;
}

size_t elib_collections_linkedlist_size(ElibLinkedList *linkedlist) {
    LinkedList *list = (LinkedList *) linkedlist;

    size_t size = 0;

    LinkedList *element = list->next;

    while(element != NULL) {
        size++;
        element = element->next;
    }

    return size;
}
