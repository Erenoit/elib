#include "hashtable.h"

#include <assert.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#define DEFAULT_SIZE 1024

typedef struct Entry {
    void         *key;
    size_t        key_size;
    void         *value;
    // TODO: maybe use an array of entries instead of linked list
    struct Entry *next;
} Entry;

typedef struct HashTable {
    // TODO: dynamic bucket size
    size_t       size;
    size_t       nitems;
    ElibHashFunc hash;
    Entry      **buckets;
} HashTable;

size_t default_hash(void *key, size_t size) {
    size_t hash = 0;

    for(size_t i = 0; i < size; i++) hash += (size_t) (key + i);

    return hash;
}

ElibHashTable *elib_collections_hashtable_create(ElibHashFunc hash_func) {
    HashTable *hashtable = malloc(sizeof(*hashtable));

    if(hashtable == NULL) return NULL;

    if(hash_func == NULL) hash_func = default_hash;

    hashtable->buckets = calloc(DEFAULT_SIZE, sizeof(hashtable->buckets));
    hashtable->size    = DEFAULT_SIZE;
    hashtable->nitems  = 0;
    hashtable->hash    = hash_func;

    return (ElibHashTable *) hashtable;
}

void elib_collections_hashtable_destroy(ElibHashTable *hashtable) {
    assert(hashtable != NULL);

    HashTable *hashtable_ = (HashTable *) hashtable;

    for(size_t i = 0; i < hashtable_->size; i++) {
        Entry *entry = (Entry *) (hashtable_->buckets + i);

        while(entry != NULL) {
            Entry *next = entry->next;

            free(entry->key);
            free(entry->value);
            free(entry);
            entry = next;
        }
    }

    free(hashtable_->buckets);
    free(hashtable_);
}

bool elib_collections_hashtable_insert(ElibHashTable *hashtable, void *key, size_t key_size,
                                       void *value) {
    assert(hashtable != NULL);

    if(key == NULL || key_size == 0 || value == NULL) return false;

    Entry *existing = elib_collections_hashtable_lookup(hashtable, key, key_size);
    if(existing != NULL) {
        existing->value = value;
        ((HashTable *) hashtable)->nitems++;
        return true;
    }

    HashTable *hashtable_ = (HashTable *) hashtable;
    size_t     hash       = hashtable_->hash(key, key_size) % hashtable_->size;

    Entry *entry    = malloc(sizeof(*entry));
    entry->key      = malloc(key_size);
    entry->key_size = key_size;
    entry->value    = value;
    entry->next     = NULL;
    memcpy(entry->key, key, key_size);

    entry->next               = hashtable_->buckets[hash];
    hashtable_->buckets[hash] = entry;

    hashtable_->nitems++;

    return true;
}

void *elib_collections_hashtable_lookup(ElibHashTable *hashtable, void *key, size_t key_size) {
    assert(hashtable != NULL);

    if(key == NULL || key_size == 0) return NULL;

    HashTable *hashtable_ = (HashTable *) hashtable;
    size_t     hash       = hashtable_->hash(key, key_size) % hashtable_->size;

    Entry *entry = hashtable_->buckets[hash];
    while(entry != NULL) {
        if(entry->key_size == key_size && memcmp(entry->key, key, key_size) == 0)
            return entry->value;

        entry = entry->next;
    }

    return NULL;
}

void *elib_collections_hashtable_remove(ElibHashTable *hashtable, void *key, size_t key_size) {
    assert(hashtable != NULL);

    if(key == NULL || key_size == 0) return NULL;

    HashTable *hashtable_ = (HashTable *) hashtable;
    size_t     hash       = hashtable_->hash(key, key_size) % hashtable_->size;

    Entry *entry = hashtable_->buckets[hash];
    Entry *prev  = NULL;
    while(entry != NULL) {
        if(entry->key_size == key_size && memcmp(entry->key, key, key_size) == 0) {
            if(prev == NULL) hashtable_->buckets[hash] = entry->next;
            else prev->next = entry->next;

            void *value = entry->value;
            free(entry->key);
            free(entry);

            hashtable_->nitems--;

            return value;
        }

        prev  = entry;
        entry = entry->next;
    }

    return NULL;
}

void *elib_collections_hashtable_keys(ElibHashTable *hashtable) {
    assert(hashtable != NULL);

    HashTable *hashtable_ = (HashTable *) hashtable;
    void     **keys       = malloc(sizeof(*keys) * hashtable_->nitems);

    size_t i = 0;
    for(size_t j = 0; j < hashtable_->size; j++) {
        Entry *entry = hashtable_->buckets[j];
        while(entry != NULL) {
            keys[i++] = entry->key;
            entry     = entry->next;
        }
    }

    return keys;
}

void *elib_collections_hashtable_values(ElibHashTable *hashtable) {
    assert(hashtable != NULL);

    HashTable *hashtable_ = (HashTable *) hashtable;
    void     **values     = malloc(sizeof(*values) * hashtable_->nitems);

    size_t i = 0;
    for(size_t j = 0; j < hashtable_->size; j++) {
        Entry *entry = hashtable_->buckets[j];
        while(entry != NULL) {
            values[i++] = entry->value;
            entry       = entry->next;
        }
    }

    return values;
}

size_t elib_collections_hashtable_size(ElibHashTable *hashtable) {
    assert(hashtable != NULL);

    return ((HashTable *) hashtable)->nitems;
}

ElibHashFunc elib_collections_hashtable_hash_func(ElibHashTable *hashtable) {
    assert(hashtable != NULL);

    return ((HashTable *) hashtable)->hash;
}
