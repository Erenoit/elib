#ifndef __ELIB_MEMORY_H__
#define __ELIB_MEMORY_H__

#include <stdlib.h>

/**
 * @brief Swaps two blocks of memory.
 * @param first The first block of memory.
 * @param second The second block of memory.
 * @param size The size of the blocks of memory.
 */
void elib_memory_memswap(void *first, void *second, size_t size);

#endif // __ELIB_MEMORY_H__
