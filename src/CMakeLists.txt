file(GLOB LIBRARY_SOURCES
    "*.c"
    "*.h"
)

add_library(${PROJECT_NAME}_static STATIC ${LIBRARY_SOURCES})
add_library(${PROJECT_NAME}_shared SHARED ${LIBRARY_SOURCES})

target_include_directories(${PROJECT_NAME}_static PUBLIC ${CMAKE_CURRENT_SOURCE_DIR})
target_include_directories(${PROJECT_NAME}_shared PUBLIC ${CMAKE_CURRENT_SOURCE_DIR})

add_subdirectory(collections)

########################################################
# Doxygen
########################################################

find_package(Doxygen)
if (DOXYGEN_FOUND)
    set(DOXYGEN_OUTPUT_DIRECTORY ${PROJECT_SOURCE_DIR}/build/doc)
    set(DOXYGEN_CONFIG_FILE "${PROJECT_SOURCE_DIR}/Doxyfile")

    message("Doxygen build started")

    add_custom_target(doc ALL
        COMMAND ${DOXYGEN_EXECUTABLE} ${DOXYGEN_CONFIG_FILE}
        WORKING_DIRECTORY ${PROJECT_SOURCE_DIR}
        COMMENT "Generating API documentation with Doxygen"
        VERBATIM )

else (DOXYGEN_FOUND)
  message("Doxygen need to be installed to generate the doxygen documentation")
endif (DOXYGEN_FOUND)
