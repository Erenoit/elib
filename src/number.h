#ifndef __ELIB_NUMBER_H__
#define __ELIB_NUMBER_H__

#include <stddef.h>
#include <stdint.h>

/******************************************************************************
 * Integer
 *****************************************************************************/

typedef int8_t    i8;
typedef int16_t   i16;
typedef int32_t   i32;
typedef int64_t   i64;
typedef ptrdiff_t isize;

typedef uint8_t  u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;
typedef size_t   usize;

typedef union i8x2 {
    i8  e[2];
    i16 v;

    struct {
        i8 x, y;
    };
} i8x2;

typedef union i16x2 {
    i16 e[2];
    i32 v;

    struct {
        i16 x, y;
    };
} i16x2;

typedef union i32x2 {
    i32 e[2];
    i64 v;

    struct {
        i32 x, y;
    };
} i32x2;

typedef union i64x2 {
    i64 e[2];

    struct {
        i64 x, y;
    };
} i64x2;

typedef union isizex2 {
    isize e[2];

    struct {
        isize x, y;
    };
} isizex2;

typedef union u8x2 {
    u8  e[2];
    u16 v;

    struct {
        u8 x, y;
    };
} u8x2;

typedef union u16x2 {
    u16 e[2];
    u32 v;

    struct {
        u16 x, y;
    };
} u16x2;

typedef union u32x2 {
    u32 e[2];
    u64 v;

    struct {
        u32 x, y;
    };
} u32x2;

typedef union u64x2 {
    u64 e[2];

    struct {
        u64 x, y;
    };
} u64x2;

typedef union usizex2 {
    usize e[2];

    struct {
        usize x, y;
    };
} usizex2;

typedef union i8x3 {
    i8 e[3];

    struct {
        i8 x, y, z;
    };

    struct {
        i8 r, g, b;
    };
} i8x3;

typedef union i16x3 {
    i16 e[3];

    struct {
        i16 x, y, z;
    };

    struct {
        i16 r, g, b;
    };
} i16x3;

typedef union i32x3 {
    i32 e[3];

    struct {
        i32 x, y, z;
    };

    struct {
        i32 r, g, b;
    };
} i32x3;

typedef union i64x3 {
    i64 e[3];

    struct {
        i64 x, y, z;
    };

    struct {
        i64 r, g, b;
    };
} i64x3;

typedef union isizex3 {
    isize e[3];

    struct {
        isize x, y, z;
    };

    struct {
        isize r, g, b;
    };
} isizex3;

typedef union u8x3 {
    u8 e[3];

    struct {
        u8 x, y, z;
    };

    struct {
        u8 r, g, b;
    };
} u8x3;

typedef union u16x3 {
    u16 e[3];

    struct {
        u16 x, y, z;
    };

    struct {
        u16 r, g, b;
    };
} u16x3;

typedef union u32x3 {
    u32 e[3];

    struct {
        u32 x, y, z;
    };

    struct {
        u32 r, g, b;
    };
} u32x3;

typedef union u64x3 {
    u64 e[3];

    struct {
        u64 x, y, z;
    };

    struct {
        u64 r, g, b;
    };
} u64x3;

typedef union usizex3 {
    usize e[3];

    struct {
        usize x, y, z;
    };

    struct {
        usize r, g, b;
    };
} usizex3;

typedef union i8x4 {
    i8 e[4];

    struct {
        i8 x, y, z, w;
    };

    struct {
        i8 r, g, b, a;
    };
} i8x4;

typedef union i16x4 {
    i16 e[4];

    struct {
        i16 x, y, z, w;
    };

    struct {
        i16 r, g, b, a;
    };
} i16x4;

typedef union i32x4 {
    i32 e[4];

    struct {
        i32 x, y, z, w;
    };

    struct {
        i32 r, g, b, a;
    };
} i32x4;

typedef union i64x4 {
    i64 e[4];

    struct {
        i64 x, y, z, w;
    };

    struct {
        i64 r, g, b, a;
    };
} i64x4;

typedef union isizex4 {
    isize e[4];

    struct {
        isize x, y, z, w;
    };

    struct {
        isize r, g, b, a;
    };
} isizex4;

typedef union u8x4 {
    u8 e[4];

    struct {
        u8 x, y, z, w;
    };

    struct {
        u8 r, g, b, a;
    };
} u8x4;

typedef union u16x4 {
    u16 e[4];

    struct {
        u16 x, y, z, w;
    };

    struct {
        u16 r, g, b, a;
    };
} u16x4;

typedef union u32x4 {
    u32 e[4];

    struct {
        u32 x, y, z, w;
    };

    struct {
        u32 r, g, b, a;
    };
} u32x4;

typedef union u64x4 {
    u64 e[4];

    struct {
        u64 x, y, z, w;
    };

    struct {
        u64 r, g, b, a;
    };
} u64x4;

typedef union usizex4 {
    usize e[4];

    struct {
        usize x, y, z, w;
    };

    struct {
        usize r, g, b, a;
    };
} usizex4;

/******************************************************************************
 * Floating Point
 *****************************************************************************/

typedef float  f32;
typedef double f64;

typedef union f32x2 {
    f32 e[2];
    f64 v;

    struct {
        f32 x, y;
    };
} f32x2;

typedef union f64x2 {
    f64 e[2];

    struct {
        f64 x, y;
    };
} f64x2;

typedef union f32x3 {
    f32 e[3];

    struct {
        f32 x, y, z;
    };

    struct {
        f32 r, g, b;
    };
} f32x3;

typedef union f64x3 {
    f64 e[3];

    struct {
        f64 x, y, z;
    };

    struct {
        f64 r, g, b;
    };
} f64x3;

typedef union f32x4 {
    f32 e[4];

    struct {
        f32 x, y, z, w;
    };

    struct {
        f32 r, g, b, a;
    };
} f32x4;

typedef union f64x4 {
    f64 e[4];

    struct {
        f64 x, y, z, w;
    };

    struct {
        f64 r, g, b, a;
    };
} f64x4;

/******************************************************************************
 * Complex
 *****************************************************************************/

#endif // __ELIB_NUMBER_H__
