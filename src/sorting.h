#ifndef __ELIB_SORTING_H__
#define __ELIB_SORTING_H__

#include <stdlib.h>

/**
 * @brief The order of two items.
 */
typedef enum Order {
    LESS    = -1,
    EQUAL   = 0,
    GREATER = 1,
} Order;

/**
 * @brief The comparison function to use.
 * @param first The first item.
 * @param second The second item.
 * @param size The size of the items.
 * @return The order of the items.
 */
typedef Order (*CompareFunc)(const void *, const void *, const size_t size);

/**
 * @brief Sorts an array using the bubble sort algorithm.
 * @param array The array to sort.
 * @param nitems The number of items in the array.
 * @param size The size of each item in the array.
 * @param compare The comparison function to use.
 * @return The sorted array.
 */
void *elib_sorting_bubble_sort(void *array, size_t nitems, size_t size, CompareFunc compare);

/**
 * @brief Sorts an array using the merge sort algorithm.
 * @param array The array to sort.
 * @param nitems The number of items in the array.
 * @param size The size of each item in the array.
 * @param compare The comparison function to use.
 * @return The sorted array.
 */
void *elib_sorting_merge_sort(void *array, size_t nitems, size_t size, CompareFunc compare);

/**
 * @brief Sorts an array using the quick sort algorithm.
 * @param array The array to sort.
 * @param nitems The number of items in the array.
 * @param size The size of each item in the array.
 * @param compare The comparison function to use.
 * @return The sorted array.
 */
void *elib_sorting_quick_sort(void *array, size_t nitems, size_t size, CompareFunc compare);

#endif // __ELIXIR_SORTING_H__
