#include "sorting.h"

#include "memory.h"

#include <stdbool.h>
#include <string.h>

static Order  default_compare(const void *first, const void *second, const size_t size);
static void   merge_sort_inner(void *array, size_t left, size_t right, size_t size,
                               CompareFunc compare);
static void   merge_sort_merge_inner(void *array, size_t left, size_t middle, size_t right,
                                     size_t size, CompareFunc compare);
static void   quick_sort_inner(void *array, size_t left, size_t right, size_t size,
                               CompareFunc compare);
static size_t quick_sort_partition_inner(void *array, size_t left, size_t right, size_t size,
                                         CompareFunc compare);

static Order default_compare(const void *first, const void *second, const size_t size) {
    int r = memcmp(first, second, size);

    if(r < 0) return LESS;
    if(r > 0) return GREATER;

    return EQUAL;
}

void *elib_sorting_bubble_sort(void *array, size_t nitems, size_t size, CompareFunc compare) {
    if(compare == NULL) compare = default_compare;

    bool sorted = true;

    for(size_t i = 0; i < nitems - 1; i++) {
        for(size_t j = 0; j < nitems - i - 1; j++) {
            if(compare(array + j * size, array + (j + 1) * size, size) > 0) {
                elib_memory_memswap(array + j * size, array + (j + 1) * size, size);
                sorted = false;
            }
        }

        if(sorted) break;
    }

    return array;
}

void *elib_sorting_merge_sort(void *array, size_t nitems, size_t size, CompareFunc compare) {
    if(compare == NULL) compare = default_compare;

    merge_sort_inner(array, 0, nitems - 1, size, compare);

    return array;
}

static void merge_sort_inner(void *array, size_t left, size_t right, size_t size,
                             CompareFunc compare) {
    if(left >= right) return;

    size_t middle = (left + right) / 2;
    merge_sort_inner(array, left, middle, size, compare);
    merge_sort_inner(array, middle + 1, right, size, compare);

    merge_sort_merge_inner(array, left, middle, right, size, compare);
}

static void merge_sort_merge_inner(void *array, size_t left, size_t middle, size_t right,
                                   size_t size, CompareFunc compare) {
    size_t left_nitems  = middle - left + 1;
    size_t right_nitems = right - middle;

    void *left_array  = malloc(left_nitems * size);
    void *right_array = malloc(right_nitems * size);

    memcpy(left_array, array + left * size, left_nitems * size);
    memcpy(right_array, array + (middle + 1) * size, right_nitems * size);

    size_t left_index  = 0;
    size_t right_index = 0;
    size_t array_index = left;

    while(left_index < left_nitems && right_index < right_nitems) {
        if(compare(left_array + left_index * size, right_array + right_index * size, size)
           == LESS) {
            memcpy(array + array_index * size, left_array + left_index * size, size);
            left_index++;
        } else {
            memcpy(array + array_index * size, right_array + right_index * size, size);
            right_index++;
        }

        array_index++;
    }

    while(left_index < left_nitems) {
        memcpy(array + array_index * size, left_array + left_index * size, size);
        left_index++;
        array_index++;
    }

    while(right_index < right_nitems) {
        memcpy(array + array_index * size, right_array + right_index * size, size);
        right_index++;
        array_index++;
    }
}

void *elib_sorting_quick_sort(void *array, size_t size, size_t width, CompareFunc compare) {
    if(compare == NULL) compare = default_compare;

    quick_sort_inner(array, 0, size - 1, width, compare);

    return array;
}

static void quick_sort_inner(void *array, size_t left, size_t right, size_t size,
                             CompareFunc compare) {
    if(left >= right) return;

    size_t pivot = quick_sort_partition_inner(array, left, right, size, compare);

    quick_sort_inner(array, left, pivot - 1, size, compare);
    quick_sort_inner(array, pivot + 1, right, size, compare);
}

static size_t quick_sort_partition_inner(void *array, size_t left, size_t right, size_t size,
                                         CompareFunc compare) {
    size_t pivot = right;
    size_t i     = left - 1;

    for(size_t j = left; j < right; j++) {
        if(compare(array + j * size, array + pivot * size, size) == LESS) {
            i++;
            elib_memory_memswap(array + i * size, array + j * size, size);
        }
    }

    elib_memory_memswap(array + (i + 1) * size, array + pivot * size, size);

    return i + 1;
}
