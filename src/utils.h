#ifndef __ELIB_UTILS_H__
#define __ELIB_UTILS_H__

#define ELIB_UNUSED(x) ((void) (x))

#define ELIB_MAX(a, b)          ((a) > (b) ? (a) : (b))
#define ELIB_MIN(a, b)          ((a) < (b) ? (a) : (b))
#define ELIB_CLAMP(x, min, max) (ELIB_MAX(ELIB_MIN((x), (max)), (min)))

#define ELIB_STRINGIFY(x) #x
#define ELIB_TOSTRING(x)  ELIB_STRINGIFY(x)

#define ELIB_ARRAY_SIZE(array) (sizeof(array) / sizeof(*array))

#if DEBUG
#define ELIB_DEBUG_PRINT(...) fprintf(stderr, __VA_ARGS__)
#else
#define ELIB_DEBUG_PRINT(...)
#endif // DEBUG

#endif // __ELIB_UTILS_H__
