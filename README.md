# Elib - Erenoit's C library

This library is a bunch of `Data Types` and `Algorithms` written in C. They may not be perfect, but
they should get the job done.

## Building
If I can configured `CMake` successfully, you can just run theses following commands:

```sh
$ cmake .
$ make
```

Then you can find the static and shared library files in `build/` directory. But you should still
need to get header files from `src/`. I couldn't find out how to do that with `CMake` yet.
